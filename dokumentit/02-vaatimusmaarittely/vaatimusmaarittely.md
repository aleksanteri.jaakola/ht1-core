# Requirement Analysis for Booking Application

- Aleksanteri Jaakola
- Document version number 1.0

## Table of Contents

- [Introduction](#introduction)
- [Client](#client)
- [The Service Description](#the-service-description)
- [Stakeholder map](#stakeholder-map)
- [Stakeholders and profiles](#stakeholders-and-profiles)
- [Customer requirements](#customer-requirements)
- [Business requirements](#business-requirements)
- [Known Risks](#known-risks)
- [User stories](#user-stories)
- [Customer Journey](#customer-journey)
- [Relevant user stories](#relevant-user-stories)
- [Properties and functionalities](#properties-and-functionalities)
- [MockUp-prototyyppi](#mockup-prototyyppi)
- [Initial User Stories](#initial-user-stories)
- [Production and technical requirements with the service](#production-and-technical-requirements-with-the-service)
  - [Relevant definitions and requirements](#relevant-definitions-and-requirements)
  - [Functional Requirements](#functional-requirements)
  - [Features](#features)
  - [Non-functional requirements](#non-functional-requirements)
  - [Security requirements](#security-requirements)
  - [Usability requirements](#usability-requirements)
  - [Testability requirements](#testability-requirements)
- [Software architecture, deployment diagram, database diagram and integrations](#software-architecture-deployment-diagram-database-diagram-and-integrations)
  - [Deployment diagram](#deployment-diagram)
  - [Database ER-diagram](#database-er-diagram)
- [Quality of service](#quality-of-service)
  - [Software acceptance tests](#software-acceptance-tests)
- [Release plan](#release-plan)
- [Standards and origins](#standards-and-origins)

## Introduction

Currently these is no booking app for the FIRMA’s computers and with the Covid19 situations we have too many people. The goal for this project is to create a mobile application to make this easier via booking each computer for a student. You can book a computer and you can see all the free and booked computers. You can also edit your booking if you don’t need it anymore.

## Client

theFIRMA, thefirma@edu.turkuamk.fi

## The Service Description

The scope of this project is to implement mobile application for the customer. A booking application for the FIRMA’s computers with React Native.
Layout design and prototype will also be implemented with Proto.io or Figma.

Database model will be also implemented.

Outcome of this project is mobile application, which gets the booking data from database and displays it properly for the user. Also user is able to book a computer with the application for desired time and date.

## Stakeholder map

```plantuml
actor stakeholder1
actor stakeholder2
actor stakeholder3
actor projectmanager
actor GDPR
actor security
actor webstandards


cloud mobile_application

stakeholder1 -- mobile_application : user
stakeholder2 -- mobile_application : user
stakeholder3 -- mobile_application : user
projectmanager -- mobile_application : manage
media -- mobile_application : inform
GDPR -- mobile_application : dataprotection
security -- mobile_application : information leaks
webstandards -- mobile_application : standards

```

## Stakeholders and profiles

|           Stakeholders/Profile           |   Further information    |
| :--------------------------------------: | :----------------------: |
| [Stakeholder-1](pohjat/stakeholder-1.md) | Represents 30 % of users |
| [Stakeholder-2](pohjat/stakeholder-2.md) | Represents 30 % of users |
| [Stakeholder-3](pohjat/stakeholder-3.md) | Represents 40 % of users |

## Customer requirements

|   requirementID   |         Type         |                      Description                      |
| :---------------: | :------------------: | :---------------------------------------------------: |
| CUSTOMER-REQ-0001 | Customer Requirement |   User wants to create profile into the application   |
| CUSTOMER-REQ-0002 | Customer Requirement |         User wants the application is simple          |
| CUSTOMER-REQ-0003 | Customer Requirement | User wants the application is usable for color blinds |
| CUSTOMER-REQ-0004 | Customer Requirement |             User wants to book a computer             |
| CUSTOMER-REQ-0005 | Customer Requirement |           User wants to edit their bookings           |
| CUSTOMER-REQ-0006 | Customer Requirement |            User wants cancel their booking            |

## Business requirements

|    VaatimusID     |        Tyyppi        |                                                   Kuvaus                                                   |
| :---------------: | :------------------: | :--------------------------------------------------------------------------------------------------------: |
| BUSINESS-REQ-0001 | Business Requirement | Login into application needs to be simple, so we can achieve wide customer base = 50% of the target group  |
| BUSINESS-REQ-0002 | Business Requirement | Cancellation of booking needs to be simple, so we can achieve wide customer base = 50% of the target group |
| BUSINESS-REQ-0003 | Business Requirement |                          Security of application needs to according to standards                           |
| BUSINESS-REQ-0004 | Business Requirement |             Application needs to be native, so we can achieve both target groups (Androis/iOS)             |

## Known Risks

- Building service to wrong target group
- Scalability for wide target group is poor
- Choosing wrong technologies
- Concentrating on wrong platform

## User stories

**User Story 1**

- [Stakeholder-1](pohjat/stakeholder-1.md) Notices there is free computer at 12:00 and makes a booking for that time.
- [Stakeholder-2](pohjat/stakeholder-2.md) Notices students are actively booking computers.
- [Stakeholder-3](pohjat/stakeholder-3.md) Student contacts admin, that is unable to cancel booking. Makes the cancellation for the user and files ticket for issue.
- [Stakeholder-1](pohjat/stakeholder-1.md) Comes sick and cancels booking.

## Customer Journey

**PLANTUML Customer journey and how is first encounter met with the application**

```plantuml

:ad about the app;
:other people's opinions about it;
if (Are the opinions positive?) then (yes)
  :Download the app;
else (no)
  :doesn't download the app
due the inflections of others;
endif
if (UI/UX good?) then (yes)
  :continue;
else (no)
  :contradictory feelings
about the application;
endif
if (registeration easy?) then (yes)
  :continue;
else (no)
  :deciding whether to
remove the app or not;
endif
if (easy to book a time?) then (yes)
  :continue;
else (no)
  :removal of the
application;
endif
:praise the application
online or/and to friends
and recommend it;
:get more users;


```

## Relevant user stories

```plantuml

rectangle Booking(students) {
rectangle Booking(students) {
Stakeholder1--(Making a booking)
Stakeholder1--(Cancelling booking)
Stakeholder1--(Editing bookings)
Stakeholder1-- (Inspection of free computers)

}

rectangle Controlling_bookings {
Stakeholder3--(Cancelling booking)
Stakeholder3--(Editing bookings)
Stakeholder3--(Removal of users)
}

rectangle Booking(teachers) {
Stakeholder2--(Inspection of free computers)
}
```

|                              Use case                               |      Sector      |               Feature UC is associated with               |
| :-----------------------------------------------------------------: | :--------------: | :-------------------------------------------------------: |
| [Use case 1 - Creating Account](pohjat/usecase-creating-account.md) | Creating Account | [Feature 1](pohjat/feature-creating-account-and-login.md) |
|   [Use case 2 - Making booking](pohjat/usecase-making-booking.md)   |  Making booking  |       [Feature 2](pohjat/ominaisuus-varaaminen.md)        |
|  [Use case 3 - Editing booking](pohjat/usecase-editing-booking.md)  | Editing booking  |                  [Feature 2](pohjat/.md)                  |
| [Use case 4 - Removing booking](pohjat/usecase-removing-booking.md) | Removing booking |          [Feature 2](pohjat/removing-booking.md)          |

## Properties and functionalities

- Relevant functionalities
  - Stakeholder1 can log in into application
  - Stakeholder1 can book a computer
  - Stakeholder1 can edit booking
  - Stakeholder1 can cancel booking
  - Stakeholder2 can see overall booking status
  - Stakeholder3 can edit user bookings
  - Stakeholder3 can cancel user booking

## MockUp-prototyyppi

![image info](./img/Figma_iteration_1.png)

## Initial User Stories

|     Use case     | Issue id |
| :--------------: | :------: |
|  Create Account  |    #1    |
| Removing booking |    #4    |
|  Making booking  |    #3    |
| Editing booking  |    #2    |
|   Remove user    |    #5    |

## Production and technical requirements with the service

|   requirementID    |             Type             |     Description     |      Feature it is associated with       |
| :----------------: | :--------------------------: | :-----------------: | :--------------------------------------: |
| SYSTEM-HW-REQ-0001 | System Technical Requirement | Expo + React Native | [xx](pohjat/ominaisuus-kirjautuminen.md) |
| SYSTEM-HW-REQ-0002 | System Technical Requirement | Firebase / similar  | [xx](pohjat/ominaisuus-kirjautuminen.md) |

### Relevant definitions and requirements

|          Id           | Description of constraint |             Category              | Responsible |
| :-------------------: | :-----------------------: | :-------------------------------: | :---------: |
| CONSTRAINT-REQ-S00000 |         Constrain         |     Regulate processor speed      |     xx      |
| CONSTRAINT-REQ-S00001 |         Constrain         |  Accessability for your own data  |     xx      |
| CONSTRAINT-REQ-S00002 |         Constrain         | Assign Data Protection Supervisor |     xx      |
| CONSTRAINT-REQ-S00003 |         Constrain         |    Constraints about platforms    |     xx      |
| CONSTRAINT-REQ-S00004 |         Constrain         |  Memory and battery constraints   |     xx      |

### Functional Requirements

|    requirementID     |          Type          |                        Description                        |             Feature it is associated with              |
| :------------------: | :--------------------: | :-------------------------------------------------------: | :----------------------------------------------------: |
| FUNCTIONAL-REQ-C0001 | Functional Requirement | [Stakeholder-1](pohjat/stakeholder-1.md) Creating Account | [Creating Account](pohjat/usecase-creating-account.md) |
| FUNCTIONAL-REQ-C0002 | Functional Requirement | [Stakeholder-1](pohjat/stakeholder-1.md) Editing booking  |   [Edit booking](pohjat/usecase-editing-booking.md)    |
| FUNCTIONAL-REQ-C0003 | Functional Requirement |  [Stakeholder-1](pohjat/stakeholder-1.md) Making booking  |    [Make booking](pohjat/usecase-making-booking.md)    |
| FUNCTIONAL-REQ-C0004 | Functional Requirement | [Stakeholder-1](pohjat/stakeholder-1.md) Removing booking |  [Remove booking](pohjat/usecase-removing-booking.md)  |
| FUNCTIONAL-REQ-C0005 | Functional Requirement |   [Stakeholder-3](pohjat/stakeholder-1.md) Remove user    |      [Remove user](pohjat/usecase-remove-user.md)      |

### Features

**Priotize the most relevant features**

- P1 = Mandatory
- P3 = Necessary
- P5 = Will be done, if needs to be done

|                                        Feature                                         | Priority |                                    Requirements/Use cases associated with feature                                     |
| :------------------------------------------------------------------------------------: | :------: | :-------------------------------------------------------------------------------------------------------------------: |
| [Feature 1 - Login and creating account](pohjat/feature-creating-account-and-login.md) |    P1    |                              [FUNCTIONAL-REQ-C0001](pohjat/usecase-creating-account.md)                               |
|                [Feature 2 - My bookings](pohjat/feature-my-bookings.md)                |    P1    | [FUNCTIONAL-REQ-C0002](pohjat/usecase-editing-booking.md), [FUNCTIONAL-REQ-C0004](pohjat/usecase-removing-booking.md) |
|               [Feature 3 - Make booking](pohjat/feature-making-booking)                |    P1    |                               [FUNCTIONAL-REQ-C0003](pohjat/usecase-making-booking.md)                                |
|                             [Feature 4 - Admin panel](xx)                              |    P3    |                                              [FUNCTIONAL-REQ-C0006](xx)                                               |

### Non-functional requirements

|    RequirementID     |            Type            |        Description         |                 Feature                  |
| :------------------: | :------------------------: | :------------------------: | :--------------------------------------: |
| PERFORMANCE-REQ-0000 | Non-Functional Performance |    Navigation is simple    | [xx](pohjat/ominaisuus-kirjautuminen.md) |
| PERFORMANCE-REQ-0001 | Non-Functional Performance | Creating account is simple |                  [xx]()                  |

### Security requirements

|   RequirementID   |          Type           |                   Description                   |              Feature              |
| :---------------: | :---------------------: | :---------------------------------------------: | :-------------------------------: |
| SECURITY-REQ-0001 | Non-Functional Security | Password dispersion with bcrypt- implementation | [xx](ominaisuus-kirjautuminen.md) |

Usability is the ease of use of an auxiliary or other manufactured object, service or environment in order to achieve a particular goal. Usability can also be used to refer to methods that measure ease of use and to learn about the principles that make the product, service or environment more accessible.

- Usability must take into account what kind of target groups this service serves. Does the target group have disabilities, colorblind, difficulty using mobile phones, etc. All these focus groups and how they see the usability of the service needs to be taken into account.

### Usability requirements

|   requirementID    |           Type           |            Description            |               Feature it is associated with                |
| :----------------: | :----------------------: | :-------------------------------: | :--------------------------------------------------------: |
| USABILITY-REQ-0000 | Non-Functional Usability | Making booking is fast and simple | [Ajanvarauksen tekeminen](pohjat/ominaisuus-varaaminen.md) |  |
| USABILITY-REQ-0001 | Non-Functional Usability |     Usability of application      |       [Feature 2](pohjat/ominaisuus-hallinnointi.md)       |

### Testability requirements

|    requirementID     |            Type            |   Description    |      Feature it is associated with       |
| :------------------: | :------------------------: | :--------------: | :--------------------------------------: |
| TESTABILITY-REQ-0000 | Non-Functional Testability | Creating Account | [xx](pohjat/ominaisuus-kirjautuminen.md) |
| TESTABILITY-REQ-0001 | Non-Functional Testability |      Login       | [xx](pohjat/ominaisuus-kirjautuminen.md) |

## Software architecture, deployment diagram, database diagram and integrations

### Deployment diagram

![image info](./img/plantuml.png)

### Database ER-diagram

```plantuml
' hide the spot
hide circle

' avoid problems with angled crows feet
skinparam linetype ortho

entity "manageUsers" as e01 {
  *id : number primary key auto_increment <<generated>>
  --
  *email: string
  --
  *Password: hashed
  --
  *datetime: data_created

}

entity "manageBookings" as e02 {
  *id : number <<generated>>
  --
  *datetime: booking_created

}

entity "Logins" as e03 {
  *string: user.email
  --
  *string hash: user.password
  --
}

e01 ||..o{ e02
e01 |o..o{ e03
e03 |o..o{e02
```

## Quality of service

![image info](./img/branch.jpeg)

## Tutorials and useful links

- Full React Course 2020 - Fundamentals, Hooks, Context API, React Router, Custom hooks - https://www.youtube.com/watch?v=4UZrsTqkcW4
- React Native Crash Course 2020 - https://www.youtube.com/watch?v=qSRrxpdMpVc

### Software acceptance tests

|                                                   Source                                                    |                             Testcase ID                             |   Description    |      Type       |
| :---------------------------------------------------------------------------------------------------------: | :-----------------------------------------------------------------: | :--------------: | :-------------: |
|                          [FUNCTIONAL-REQ-0001](pohjat/ominaisuus-kirjautuminen.md)                          |           [Login](pohjat/facebook-kirjautuminen-testi.md)           | Creating Account | Acceptance test |
| [Feature 1](pohjat/ominaisuus-kirjautuminen.md), [FUNCTIONAL-REQ-C0002](pohjat/ominaisuus-kirjautuminen.md) |         [Create account](pohjat/google-hyvaksyntatesti.md)          |      Login       | Acceptance test |
|                           [FUNCTIONAL-REQ-C0003](pohjat/ominaisuus-varaaminen.md)                           |      [Make a booking](pohjat/kirjautuminen-hyvaksyntatesti.md)      |  Making booking  | Acceptance test |
|                                [Feature 3](pohjat/ominaisuus-poistaminen.md)                                | [Remove booking](pohjat/Ajanvarauksen-tekeminen-hyvaksyntatesti.md) | Editing booking  | Acceptance test |

## Release plan

```plantuml
Project starts the 2020-09-00
[Version v1.0 EarlyAdopter] Starts 2020-09-18 and ends 2020-12-31
[Design Phase] Starts 2020-09-18 and ends 2020-10-04
[Feature 1 v 1.0] Starts 2020-10-05 and ends 2020-10-17
[Feature 2 v 1.0] Starts 2020-10-18 and ends 2020-10-31
[Feature 3 v 1.1] Starts 2020-11-01 and ends 2020-11-14
[Feature 4 v 1.0] Starts 2020-11-15 and ends 2020-11-28
[Accceptance Testing ] Starts 2020-11-29 and ends 2020-12-05
[A/B testing] Starts 2020-12-06 and ends 2020-12-12
[User testing] Starts 2020-12-13 and ends 2020-12-19
[Final tuning] Starts 2020-12-20 and ends 2020-12-31
```

|                   Feature/functionality                   | Version | Available for testing | Release |
| :-------------------------------------------------------: | :-----: | :-------------------: | :-----: |
| [Feature 1](pohjat/feature-creating-account-and-login.md) |   1.0   |      11.15.2020       |  V1.0   |
|           [Feature 2](pohjat/making-booking.md)           |   1.0   |      12.10.2020       |  V1.0   |

## Standards and origins

|     ID      |                                                      Link                                                      |        Header        |
| :---------: | :------------------------------------------------------------------------------------------------------------: | :------------------: |
| JHS 165 ICT | http://www.jhs-suositukset.fi/c/document_library/get_file?uuid=b8118ad7-8ee4-459a-a12b-f56655e4ab9d&groupId=14 | Requirement Analysis |
