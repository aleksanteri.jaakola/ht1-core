# Feature:


| | |
|:-:|:-:|
| Feature ID | F0001 |
| Sector, feature is associated with | Creating Account |
| Ominaisuuden vastuuhenkilö | xx |
| Status | pending |

### Description

How stakeholders create account for the application

### Constraints and requirements associated with feature


| | |
|:-:|:-:|
| [Use Case 1](pohjat/usecase-creating-account.md) | Creating Account |


### Alustavat käyttäjätarinat (User Storys)

* #35 #36


### Käyttöliittymänäkymä/mock 

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FZt1zVF60ZcAwsvv0qynsYA%2FUntitled%3Fnode-id%3D8%253A2&chrome=DOCUMENTATION" allowfullscreen></iframe>

### Testing / mahdolliset hyväksyntä kriteerit 


| Testitapaus  | Testin lähde  | Kuka vastaa  |
|:-: | :-:|:-:|
| [Hyväksyntätesti 1](Ajanvarauksen-poistaminen-hyvaksyntatesti.md)  |  YK0293  |  Pekka Rinne |





