# Use Case:

## Use Case 

```plantuml

rectangle Remove_user {
Stakeholder3--(Search for spesific user)
Stakeholder3--(Flatlist with user email)
Stakeholder3--(Click 'remove' to remove user)
}

```


* Laatija: Aleksanteri Jaakola
* Date / Versio 16.10.2020 / 1.0 
* Process sector: Remove user
	
**User roles**	

1. Stakeholder 3

**Preliminary knowledge/terms**	

1. Open application
2. Login with admin credentials

**Description of the use case**

1. When you have signed in in with admin credentials open menu from top right
2. Navigate to 'remove user'
3. Search or scroll for spesific user to be deleted
4. Press 'remove' button to remove intended user from the database


	
**The outcome**	

* Searching for spesific user was successful
* Removing user will remove it from database


**Test cases** 

* Perform as many times as possible





