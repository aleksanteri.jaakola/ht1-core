| | |
|:-:|:-:|
| Testitapauksen kuvaus | Facebook registeröinti  |
| Testitapaus ID | G_login_0293 |
| Testitapauksen suunnittelija | Pekka Sirviö | 
| Luontipvm | 5.6.2020 |
| Luokitus | Hyväksyntätesti / Acceptance Test |


**Päivityshistoria**

* versio 0.1 Pohja

**Testin kuvaus / tavoite**

* Testin tavoitteena on, että registeröinti Googlen avulla on toiminnallinen. 

**Linkit vaatimuksiin tai muihin lähteisin**

  * Käyttötapaus: [Use Case](kayttotapaus-kirjautuminen.md)
  * Ominaisuus: [Use Case](ominaisuus-kirjautuminen.md)


**Testin alkutilanne (Pre-state)** 

* Testin suorittaminen alkaa siitä, painetaan 'Google'- painiketta. 


**Testiaskeleet (Test Steps)**

1. askel --> Painetaan 'Google' painiketta
2. askel --> Aukeaa Google ikkuna omilla Google tiedoilla
3. askel1 --> Valitaan mitä google profiilia käytetään
4. aksel --> Google pyytää oikeutta käyttää julkisia tietoja kyseiseen sovellukseen
5. askel --> Registeröinti onnistui Google avulla

**Testin lopputilanne (End-State)**

Testin lopputilanne on se kun registeröinti onnistui Google rajapinnan avulla


**Testin tuloksen määrittyminen (Pass/Fail Criteria)**

Millä ehdoin testin tulos voidaan hyväksyä ja millä se hylätään?

* PASS ehto:
 - Rajapintapyyntö palauttaa Google kirjautumis sivun
 - Kirjautumisen tarkitus, oletko kirjautunut vai et
 - Voi valita mitä Google profiilia käyttää. Esimerkiksi pekka.suoni@gmail.fi vai tuomo.setä@gmail.fi
* FAIL ehto:
 - Jos jokin yllämainituista ei läpäise testejä
