# Profile:
* Stakeholder 2

### reference/segment:

* This profile represents teachers

### Persona/description of stakeholder

Teachers, atleast on theFIRMA have great skills on IT. 


**name and background**

* Teachers will use this application primarily as admins. They will see the overall booking status and who booked and what. 

### Motive to use application

* Teachers use this application so they can information when is busy day for students to book a computer and when it's not. 


### Values

* Speed, simplicity and functionality

### Tools and skills etc

*   We must also consider the possibility that this group may have people, who struggles with the use of it
* For example color blindness

