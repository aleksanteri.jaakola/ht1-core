# Hyväksyntätesti:


| | |
|:-:|:-:|
| Testitapauksen kuvaus | Ajanvarauksen tekeminen   |
| Testitapaus ID | DK0293 |
| Testitapauksen suunnittelija | Pekka Sieni | 
| Luontipvm | 5.6.2020 |
| Luokitus | Hyväksyntätesti / Acceptance Test |


**Päivityshistoria**

* versio 0.1 Pohja

**Testin kuvaus / tavoite**

* Testin tavoitteena on, että ajanvarauksen tekeminen on toiminnallinen

**Linkit vaatimuksiin tai muihin lähteisin**

* Käyttötapaus: [Use Case](kayttotapaus-varaaminen.md) 
* Ominaisuus: [Ominaisuus](ominaisuus-varaaminen.md) 

**Testin alkutilanne (Pre-state)** 

* Kirjautuminen/vapaat ajat
* Testin alkutilanne alkaa siitä kun kirjaudutaan sisään. Tämän jälkeen painetaan painiketta 'Vapaat ajat', josta aukeaa lista vapaista ajoista. 

**Testiaskeleet (Test Steps)**

1. askel --> Navigointi 'vapaat ajat' kohtaan on toiminnallinen
2. askel --> Vapaat ajat näkyvät käyttöliittymässä oikein (Aikajärjestyksessä, ylhäällä ensimmäisenä on ensimmäinen vapaa aika)
3. askel --> Painetaan jotakin vapaata aikaa, joka ns. lukitsee tämän ajan
4. askel --> Kun jokin aika on valittu painetaan näppäintä 'Varaa aika'
5. askel --> Tulee ilmoitus käyttöliittymässä 'Aika on varattu onnistuneesti'
6. askel --> Tämän jälkeen tulee varmistusviesti puhelinnumeroon, että varaus on tehty ja varauksen tiedot. 



**Testin lopputilanne (End-State)**

Testin lopputilanne on se, kun kaikki testiaskeleet on käyty läpi ja varaus on tehty onnistuneesti. 


**Testin tuloksen määrittyminen (Pass/Fail Criteria)**

> Millä ehdoin testin tulos voidaan hyväksyä ja millä se hylätään?

* PASS ehto:
 - Navigointi on toiminnallinen
 - Varaukset löytyvät aikajärjestyksessä käyttöliitymästä
 - Varauksen voi lukita
 - 'Varaa aika' painike toimii suunitellusti
 - Ilmoitus käyttöliittymässä, että 'Aika on varattu onnistuneesti'
 - Tekstiviestivarmitus tietoineen tulee puhelimeen
* FAIL ehto:
 - Jos jokin yllämainituista ei läpäise testejä
