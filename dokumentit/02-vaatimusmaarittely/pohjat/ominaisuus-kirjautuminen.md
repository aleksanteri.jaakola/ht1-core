# Ominaisuus:


| | |
|:-:|:-:|
| Ominaisuus ID | Login_001 |
| Osajärjestelmä, mihin ominaisuus liittyy | Kirjautuminen |
| Ominaisuuden vastuuhenkilö | Pekka Terasniska |
| Status | Odottaa hyväksyntää |

### Kuvaus

Tämän ominaisuuden tarkoituksena on pilkkoa osiin, miten kirjautuminen sisään sekä Facebook ja Google registeröinti toteutetaan. 


### Ominaisuuteen liittyvät rajaukset, vaatimukset käyttötapaukset


| | |
|:-:|:-:|
| [Use Case 1](kayttotapaus-kirjautuminen.md) | Kirjautuminen sisään|


### Alustavat käyttäjätarinat (User Storys)

* #27 #30 #31


### Käyttöliittymänäkymä/mock 

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FZt1zVF60ZcAwsvv0qynsYA%2FUntitled%3Fnode-id%3D1%253A2&chrome=DOCUMENTATION" allowfullscreen></iframe>

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FZt1zVF60ZcAwsvv0qynsYA%2FUntitled%3Fnode-id%3D8%253A50&chrome=DOCUMENTATION" allowfullscreen></iframe>

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FZt1zVF60ZcAwsvv0qynsYA%2FUntitled%3Fnode-id%3D8%253A83&chrome=DOCUMENTATION" allowfullscreen></iframe>

### Testaus / mahdolliset hyväksyntä kriteerit 


| Testitapaus  | Testin lähde  | Kuka vastaa  |
|:-: | :-:|:-:|
| [Hyväksyntätesti 1](kirjautuminen-hyvaksyntatesti.md)  | LTD0293 | Pekka Luode |







