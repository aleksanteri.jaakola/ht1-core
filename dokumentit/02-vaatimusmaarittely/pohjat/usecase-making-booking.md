# Use Case:

## Use Case 

```plantuml

rectangle Making-Booking {
Stakeholder1--(View bookings)
Stakeholder1--(Select desired booking and confirm)
}

rectangle Making-Booking {
Stakeholder2--(View bookings)
}
```


* Laatija: Aleksanteri Jaakola
* Date / Versio 4.10.2020 / 1.0 
* Process sector: Make booking
	
**User roles**	

1. Stakeholder 1 and 3

**Preliminary knowledge/terms**	

1. Open application
2. Navigate to bookings

**Description of the use case**

1. Open application
2. Navigate to 'Make booking'
3. View bookings
4. Select desired date and time
5. Select desired computer to book. 
6. Click next

**Exceptions**
 
* Admins do not get full access to users personal data

	
**The outcome**	

* Making a booking was successful
* Database updated successfully


**Test cases** 

* Perform as many times as possible

