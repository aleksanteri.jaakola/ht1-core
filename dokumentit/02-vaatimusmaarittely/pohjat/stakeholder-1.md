# Profile:

* Stakeholder 1

### reference group/segment:

* This profile represents students

### Persona/description of stakeholder

* This stakeholder group consists students and age varies. 


**name and background**

* Students will use this booking application to book a time for computer

### Motive to use application 

* Motive for this group is to use the application, to book a computer for their use. 


### Values

* Speed, simplicity, functionality

### Tools and skills etc

*  We must also consider the possibility that this group may have people, who struggles with the use of it
* For example color blindness

