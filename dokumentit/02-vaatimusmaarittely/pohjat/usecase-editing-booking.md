# Use Case:

## Use Case 

```plantuml

rectangle Making-Booking {
Stakeholder1--(View user bookings)
Stakeholder1-- (Edit booking)
}

rectangle Making-Booking {
Stakeholder3--(Edit booking)
Stakeholder3--(View user bookings)

}
```


* Laatija: Aleksanteri Jaakola
* Date / Versio 4.10.2020 / 1.0 
* Process sector: Edit booking
	
**User roles**	

1. Stakeholder 1 and 3

**Preliminary knowledge/terms**	

1. Open application
2. Navigate to profile


**Description of the use case**

1. Open application
2. Navigate to profile
3. View my bookings
4. Select desired booking and edit it

**Exceptions**
 
* Admins do not get full access to users personal data

	
**The outcome**	

* Editing booking was successful
* When editing was done it updated it to database properly


**Test cases** 

* Perform as many times as possible




