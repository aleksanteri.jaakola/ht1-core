# Use Case:

## Use Case 

```plantuml

rectangle Register {
Stakeholder1--(Register)
Stakeholder1--(Log in)
}

rectangle Register {
Stakeholder2--(Log in)

}

rectangle Register {
Stakeholder3--(Log in)

}

rectangle Register {
Stakeholder3--(Add admin priviledges from database)

}

rectangle Register {
Stakeholder2--(Request admin priviledges)

}

```


* Laatija: Aleksanteri Jaakola
* Date / Versio 3.10.2020 / 1.0 
* Process sector: Create Account
	
**User roles**	

1. Stakeholder 1-3

**Preliminary knowledge/terms**	

1. Open application
2. When application have downloaded, first frame is Login / Register
3. Login with your credentials or create account

**Description of the use case**

1. If you do not have account click 'REGISTER HERE' button
2. Give your email address
3. Give your password
4. Comfirm password
5. Click 'Sign Up' button to create an account


**Exceptions**
 
* Admins do not get full access to users personal data

	
**The outcome**	

* Login was successful
* Creating account was successful


**Test cases** 

* Perform as many times as possible





