# Hyväksyntätesti:


| | |
|:-:|:-:|
| Testitapauksen kuvaus | Kirjautuminen sisään |
| Testitapaus ID | TD0293 |
| Testitapauksen suunnittelija | Pekka Luode | 
| Luontipvm | 5.6.2020 |
| Luokitus | Hyväksyntätesti / Acceptance Test |


**Päivityshistoria**

* versio 0.1 Pohja

**Testin kuvaus / tavoite**

* Testin tavoitteena on, että sisään kirjautuminen toimii oikein.  

**Linkit vaatimuksiin tai muihin lähteisin**
  
* Käyttötapaus: [Use Case](kayttotapaus-kirjautuminen.md) 
* Ominaisuus: [Ominaisuus](ominaisuus-kirjautuminen.md) 

**Testin alkutilanne (Pre-state)** 

* Testin alkutilanne on se, kun painetaan sovelluskuvaa kännykästä ja sovellus avautuu. Tämän jälkeen aukeaa kirjautumis sivu

**Testiaskeleet (Test Steps)**

1. askel --> Tarkista, että käyttäjätunnus löytyy tietokannasta
2. askel --> Tarkista, että käyttäjätunnus ja salasana mätsäävät
3. askel --> Tarkista, että 'Kirjaudu sisään' painike on toiminnallinen
4. askel --> Jos käyttäjätunnusta ei löydy, anna virheilmoitus 'Tälläistä käyttätunnusta ei ole olemassa'
5. askel --> Jos Käyttäjätunnus tai salasana on virheellinen, anna virheilmoitus 'Käyttäjätunnus tai salasana on virheellinen'
6. askel --> Tarkista, että 'Kirjaudu sisään' painike vie käyttäjän sovelluksen etusivulle, josta löytyvät painikkeet 'Oma profiili', 'Vapaat ajat' ja 'Varatut ajat'



**Testin lopputilanne (End-State)**

Testin lopputilanne on se, kun kaikki askeleet on käyty läpi ja käyttäjä on kirjautunut sisään


**Testin tuloksen määrittyminen (Pass/Fail Criteria)**


* PASS ehto:
 - Käyttäjätunnus löytyy tietokannasta
 - Käyttätunnus ja salasana mätsäävät
 - 'Kirjaudu sisään' painike on toiminnallinen
 - Virheilmoitus jos käyttätunnusta ei löydy
 - Virheilmoitus jos käyttäjätunnus ja salasana eivät mätsää
 - 'Kirjaudu sisään' painike vie sovelluksen etusivulle
* FAIL ehto:
 - Jos jokin yllämainituista ei läpäise testejä
