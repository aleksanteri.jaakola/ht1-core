# Hyväksyntätesti:


| | |
|:-:|:-:|
| Testitapauksen kuvaus | Ajanvarauksen poistaminen   |
| Testitapaus ID | YK0293 |
| Testitapauksen suunnittelija | Pekka Sieni | 
| Luontipvm | 5.6.2020 |
| Luokitus | Hyväksyntätesti / Acceptance Test |


**Päivityshistoria**

* versio 0.1 Pohja

**Testin kuvaus / tavoite**

* Testin tavoitteena on, että ajanvarauksen poistaminen on toiminnallinen

**Linkit vaatimuksiin tai muihin lähteisin**

* Käyttötapaus: [Use Case](kayttotapaus-poistaminen.md) 
* Ominaisuus: [Use case](ominaisuus-poistaminen.md) 

**Testin alkutilanne (Pre-state)** 

* Kirjautuminen/varatut ajat
* Testin alkutilanne alkaa siitä kun kirjaudutaan sisään. Tämän jälkeen painetaan painiketta 'varatut ajat', josta aukeaa lista omista varatuista ajoista

**Testiaskeleet (Test Steps)**

1. askel --> Navigointi 'varatut' kohtaan on toiminnallinen
2. askel --> Varatut näkyvät käyttöliittymässä
3. askel --> Painetaan jotakin varattua aikaa
4. askel --> Kun jokin varattu aika on valittu painetaan näppäintä 'Peru aika'
5. askel --> Tulee ilmoitus käyttöliittymässä 'Varattu aika on peruttu onnistuneesti'
6. askel --> Tämän jälkeen tulee varmistusviesti puhelinnumeroon, että 'Olette peruneet valitsemanne ajan (Sisältää varatun ajan tietoja, mm. kuka perui ajan, kenelle työntekijälle ajanvaraus oli tehty ja kellonaika)



**Testin lopputilanne (End-State)**

Testin lopputilanne on se, kun kaikki testiaskeleet on käyty läpi ja varaus on poistettu on tehty onnistuneesti. 


**Testin tuloksen määrittyminen (Pass/Fail Criteria)**

> Millä ehdoin testin tulos voidaan hyväksyä ja millä se hylätään?

* PASS ehto:
 - Navigointi on toiminnallinen
 - Varatut ajat löytyvät käyttöliitymästä
 - Varatun ajan voi lukita
 - 'Peru aika' painike toimii suunitellusti
 - Ilmoitus käyttöliittymässä, että 'Olette peruneet valitsemanne ajan'
 - Tekstiviestivarmitus perutusta ajasta tulee puhelimeen
* FAIL ehto:
 - Jos jokin yllämainituista ei läpäise testejä
