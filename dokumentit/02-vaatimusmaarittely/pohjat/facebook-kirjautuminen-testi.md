| | |
|:-:|:-:|
| Testitapauksen kuvaus | Facebook registeröinti  |
| Testitapaus ID | login_02931 |
| Testitapauksen suunnittelija | Pekka Sirviö | 
| Luontipvm | 5.6.2020 |
| Luokitus | Hyväksyntätesti / Acceptance Test |


**Päivityshistoria**

* versio 0.1 Pohja

**Testin kuvaus / tavoite**

* Testin tavoitteena on, että kirjautuminen käyttäjätunnuksella ja salasanalla on toiminallinen

**Linkit vaatimuksiin tai muihin lähteisin**

  * Käyttötapaus: [Use Case](kayttotapaus-kirjautuminen.md)
  * Ominaisuus: [Use Case](ominaisuus-kirjautuminen.md)


**Testin alkutilanne (Pre-state)** 

* Testin alkutilanne on se, kun ollaan avattu sovellus kännykästä ja aukeaa kirjautumis sivu


**Testiaskeleet (Test Steps)**

1. askel --> Kirjoitetaan käyttätunnus 'Käyttätunnus' kohtaan
2. askel --> Kirjoitetaan salasana 'Sanasana' kohtaan
3. askel --> Painetaan painiketta 'Kirjaudu sisään'
4. askel --> Jos käyttäjätunnusta ei löydy, tulee virheilmoitus 'Tälläistä käyttäjätunnusta ei ole olemassa'
5. askel --> Jos käyttätunnus tai salasana on väärin tulee virheilmoitus 'Käyttätunnuksesi tai salasanasi on väärin'
6. askel --> Kun kirjautuminen on onnistunut, käyttäjä pääsee sovelluksen ns. etusivulle, josta löytyvät painikkeet 'Oma profiili', 'Vapaat ajat' ja 'Varatut ajat'

**Testin lopputilanne (End-State)**

Testin lopputilanne on se, kun ollaan onnistuneesti kirjauduttu sisään


**Testin tuloksen määrittyminen (Pass/Fail Criteria)**

Millä ehdoin testin tulos voidaan hyväksyä ja millä se hylätään?

* PASS ehto:
 - Kirjaudu sisään painike toimii oikealla käyttäjätunnuksella ja salasanalla
 - Jos ei löydy tulee virheilmoitus 'Tälläistä käyttäjätunnusta ei ole olemassa'
 - Jos käyttäjätunnus tai salasana on väärin tulee virheilmoitus 'Käyttäjätunnuksesi tai salasanasi on väärin'
* FAIL ehto:
 - Jos jokin yllämainituista ei läpäise testejä
