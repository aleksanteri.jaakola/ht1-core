# Feature: My bookings


| | |
|:-:|:-:|
| Feature ID | F0040 |
| Sector, feature is associated with | My bookings |
| Ominaisuuden vastuuhenkilö | xx |
| Status | pending |

### Description

How stakeholders create account for the application

### Constraints and requirements associated with feature


| | |
|:-:|:-:|
| [Use Case 1](pohjat/usecase-usecase-editing-booking.md) | Editing booking |
| [Use Case 2](pohjat/usecase-usecase-removing-booking.md) | Removing booking |


### Alustavat käyttäjätarinat (User Storys)

* #2 #3 #4


### Käyttöliittymänäkymä/mock 

![image info](./img/removebooking.png)

### Testing / mahdolliset hyväksyntä kriteerit 


| Testitapaus  | Testin lähde  | Kuka vastaa  |
|:-: | :-:|:-:|
| [Hyväksyntätesti 1](Ajanvarauksen-poistaminen-hyvaksyntatesti.md)  |  YK0293  |  Pekka Rinne |
