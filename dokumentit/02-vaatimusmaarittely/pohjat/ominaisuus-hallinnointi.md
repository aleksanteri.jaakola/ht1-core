# Ominaisuus:


| | |
|:-:|:-:|
| Ominaisuus ID | H0291 |
| Osajärjestelmä, mihin ominaisuus liittyy | Profiilin hallinnointi |
| Ominaisuuden vastuuhenkilö | Mauri Myyrä |
| Status | hyväksytty |

### Kuvaus

Tämän ominaisuuden tarkoituksena on pilkkoa osiin, miten sidosryhmät 1-3, Hallinto_1 voivat hallita palvelua. Miten he voivat tarkastella, muokata vapaita sekä varattuja aikoja. 


### Ominaisuuteen liittyvät rajaukset, vaatimukset käyttötapaukset

| | |
|:-:|:-:|
| [Use Case 1](kayttotapaus-hallinnointi.md) | Käyttötapaus hallinnointi |


### Alustavat käyttäjätarinat (User Storys)


* #28 #29 #32


### Käyttöliittymänäkymä/mock 

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FZt1zVF60ZcAwsvv0qynsYA%2FUntitled%3Fnode-id%3D2%253A55&chrome=DOCUMENTATION" allowfullscreen></iframe>

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FZt1zVF60ZcAwsvv0qynsYA%2FUntitled%3Fnode-id%3D8%253A2&chrome=DOCUMENTATION" allowfullscreen></iframe>

### Testaus / mahdolliset hyväksyntä kriteerit 


| Testitapaus  | Testin lähde  | Kuka vastaa  |
|:-: | :-:|:-:|
| [Hyväksyntätesti 1]()  | JP-3948 |  Pekka Rinne |






