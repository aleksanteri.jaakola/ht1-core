# Use Case:

## Use Case 

```plantuml

rectangle Making-Booking {
Stakeholder1--(View user bookings)
Stakeholder1-- (Remove booking)
}

rectangle Making-Booking {
Stakeholder3--(View user bookings)
Stakeholder3--(Remove booking)

}
```


* Laatija: Aleksanteri Jaakola
* Date / Versio 4.10.2020 / 1.0 
* Process sector: Remove booking
	
**User roles**	

1. Stakeholder 1 and 3

**Preliminary knowledge/terms**	

1. Open application
2. Navigate to bookings

**Description of the use case**

1. Open application
2. Navigate to profile
3. Navigate my bookings
4. Select desired booking and swipe to left
5. Select bin icon to remove it. 

**Exceptions**
 
* Admins do not get full access to users personal data

	
**The outcome**	

* Removing a booking was successful
* Database updated successfully upon removal


**Test cases** 

* Perform as many times as possible

