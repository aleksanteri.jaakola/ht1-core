# Tietoa tekijästä ja toimeksiannosta

**HUOMIO! Tuotettu vaatimusmäärittely on pelkästään harjoitustyö!**

Harjoitustehtävän sisältämä dokumentaatio ei ole mitään tekeimistä alkuperäisen hankintailmoituksen  kanssa!

![](https://openclipart.org/image/300px/svg_to_png/249638/AbstractDesign288.png)


Tervetuloa tutustumaan tekemääni vaatimusmäärittelydokumenttiin!

Tämä on harjoitustehtävä JAMK IT-instituutin kurssille TTOS0100

Löydät opintokokonaisuuden osoitteesta: http://ttos0100.pages.labranet.jamk.fi/amk/

* Kevät 2020
* Tekijä: Aleksanteri Jaakola
* Aihe: TA-2020-9 

**Vaatimusmäärittelyn aiheina on käytetty [hilma.fi](http://hilma.fi) palvelussa vuonna 2020 olleita hankintailmoituksia**





